#coding:utf-8

import os
import math
import pydub
import speech_recognition
import argparse

current_dir = os.getcwd()


def filename_of_sound(filename_relative):
    # full sound filename's pathname maybely.
    maybe_file = os.path.join(current_dir, filename_relative + '')

    if os.path.isfile(maybe_file) :
        sound_filename = maybe_file
        return sound_filename
    else :
        print(maybe_file + ' is not turepath')
        return None
 

def mp3_file_to_wav_file(mp3_filename):
    full_wav_filename = os.path.splitext(mp3_filename)[0] + ".wav"

    sound = pydub.AudioSegment.from_mp3(mp3_filename)
    sound.export(full_wav_filename, format="wav")

    return full_wav_filename


def wav_file_multiple_split(sound_pathname, second_per_split):
    audio = pydub.AudioSegment.from_wav(sound_pathname)
    split_ms = second_per_split * 1000 # ms

    total_second = math.ceil(audio.duration_seconds)
    pathnames = []
    
    for i in range(0, total_second, second_per_split):
        split_pathname = \
            os.path.splitext(sound_pathname)[0] + "_" + \
            str(i) + "-" + str(i+second_per_split) + \
            os.path.splitext(sound_pathname)[1]
        split_audio = audio[i*1000:(i+second_per_split)*1000]
        split_audio.export(split_pathname, format="wav")
        pathnames.append(split_pathname)

    return pathnames
    
def wav_file_to_text(wav_filename, lang):
    recogni = speech_recognition.Recognizer()
    with speech_recognition.AudioFile(wav_filename) as source:
        audio = recogni.record(source)
    recognized_text = recogni.recognize_google(audio, language=lang)
    
    return recognized_text



class Recognization:
    def __init__(self, filename_sound):
        # 
        self.filename_sound = filename_of_sound(filename_sound)
        if self.filename_sound != None :
            self.filename_sound_extension = os.path.splitext(self.filename_sound)[1]
            # 
            self.an_filename_wav = self.copy_sound_unless_wav()
            #
            self.filename_wavs  = self.split_wav_files()
            
        self.text = None

    def print_sound_filename(self):
        return "# " + str(os.path.relpath(recognizate.filename_sound))
        
    def copy_sound_unless_wav(self):
        # extension
        self.an_filename_wav = None
        if   self.filename_sound_extension == ".wav" :
            self.an_filename_wav = full_sound_filename
        elif self.filename_sound_extension == ".mp3" :
            self.an_filename_wav = mp3_file_to_wav_file(self.filename_sound)
        else :
            pass
        
        return self.an_filename_wav

    def split_wav_files(self):
        self.filename_wavs = None
        
        if self.an_filename_wav :
            self.filename_wavs = wav_file_multiple_split(self.an_filename_wav, 30)
        return self.filename_wavs
    

    def recognizate(self, lang):
        if self.filename_wavs:
            self.text = ""
            # !
            print(recognizate.print_sound_filename())
            
            for sepd_wav_pathname in self.filename_wavs:
                recog_text = wav_file_to_text(sepd_wav_pathname, lang)
                self.text = self.text + recog_text + "\n"
                # !
                print(recog_text)
        else:
            pass
        
        return self.text

    def clean_tmp_file(self):
        if self.filename_sound_extension != ".wav" and os.path.exists(self.an_filename_wav):
            os.remove(self.an_filename_wav)
        for sepd_wav_pathname in self.filename_wavs:
            os.remove(sepd_wav_pathname)


def init_recognization_by_filenames_of_sounds(args_voices):
    if args.voices == None:
        return False
    if isinstance(args_voices, list) :
        recognization_list = []

        for filename in args_voices :
            recognization = Recognization(filename)
            if recognization.filename_sound != None : 
                recognization_list.append(recognization)
                
            print('file to recognize:',  os.path.relpath(recognization.filename_sound))
        
        return recognization_list
        

def filename_of_export(args_text, first_sound_filename):
    if args_text == None :
        return os.path.join(current_dir, os.path.splitext(first_sound_filename)[0] + '.txt')
    elif isinstance(args.text, str) :
        return os.path.join(current_dir, args.text)

    
if __name__ == '__main__':

    # command argument
    help_of_lang = """language. such as en-US, ja-JP, fr-FR, de-DE, dl-GR, ru-RU, zh, and so on ...\n
    source: <https://cloud.google.com/speech-to-text/docs/languages>.
    default is ja-JP."""
    
    parser = argparse.ArgumentParser(description='Voices to Text tool.')
    parser.add_argument('--voices', type=str, nargs='+',
                        help='Sound Files. mp3 and wav extentdion is supporting now.')
    parser.add_argument('--text', help='File to save recognized text.')
    parser.add_argument('--lang', default="ja-JP", help=help_of_lang)
    
    args = parser.parse_args()
    # print(args)

    if (args.voices is None):
        parser.print_help()
        print()
        print('!! --voices argument is None !!')
        quit()


    try:
        # init recognizate list by filenames of voice
        recognizate_list = init_recognization_by_filenames_of_sounds(args.voices)

        # check if there is bad pathname
        if len(recognizate_list) != len(args.voices) :
            print('There is bad pathname for voices.')
            quit()
        
        # export filename
        export_filename = filename_of_export(args.text, recognizate_list[0].filename_sound)

        # print status
        print("file to export text: ", export_filename)
        print("")

        # recoginze
        if recognizate_list:
            print('Recognizing ... ')
            for recognizate in recognizate_list:
                recognizate.recognizate(args.lang)
                print("")
    
    except:
        print("Interrupt ...")
        pass
    
    else:
        # save text
        if recognizate_list:
            print('Saving recognized text.')
            text_list_whole = []
        
            for recognizate in recognizate_list:
                text_list_whole = \
                    text_list_whole + [recognizate.print_sound_filename(), recognizate.text, ""]

            text_whole = "\n".join(text_list_whole)

            with open(export_filename, 'w', encoding='utf-8') as f:
                f.write(text_whole)
            print('recognized text is saved to', os.path.relpath(export_filename))

    finally:
        # close
        if recognizate_list:
            for recognizate in recognizate_list:
                recognizate.clean_tmp_file()
        print("Completed!")
    
    quit()

    
